# Discourse for Book Brawl #

We need the forum and messaging system working for Book Brawl. Ideally, the username and password, and avatar will get carried over from their Book Brawl account. 

### Install ###

* https://meta.discourse.org/t/beginners-guide-to-install-discourse-on-ubuntu-for-development/14727

- Start with steps clone disclosure and on.

### Mail Catcher ###

* https://github.com/sj26/mailcatcher

### Create User ###

* https://meta.discourse.org/t/how-to-manually-add-users-and-email-in-console-mode/26669